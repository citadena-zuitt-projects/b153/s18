let trainer = {
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
}

trainer.Name = "Ash Ketchum"
trainer.age = 10
trainer.friends = {hoenn: ["May", "Max"], kanto:["Brock", "Misty"]}
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"]

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.Name);
console.log("Result of talk method:");
trainer.talk()
